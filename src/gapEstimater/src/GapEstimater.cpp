#include "GapEstimater.h"

GapEstimater::GapEstimater(void){
	m_contigs = new vector<Contig*>; 
	m_edges = new list<PET*>;
	m_contigsName = new map<string, Contig*>;
}

GapEstimater::~GapEstimater(void){
	for( vector<Contig*>::iterator cIter = m_contigs->begin(); cIter != m_contigs->end(); cIter++ ){
		delete *cIter;
	}
	m_contigs->clear();
	delete m_contigs;

	for( list<PET*>::iterator eIter = m_edges->begin(); eIter != m_edges->end(); eIter++ ){
		delete *eIter;
	}
	m_edges->clear();
	delete m_edges;

	m_contigsName->clear();
	delete m_contigsName;
}

int GapEstimater::ReadScaffolds( string scafFile ){
	ifstream scafReader( scafFile.c_str() );
	
	if( scafReader == NULL ){
		cout<<"Cannot open "<<scafFile<<" file\n";
		return -1;
	}

	string line = "";
	
	int num = 0;
	while( getline( scafReader, line ) ){
		if( line.substr( 0, 1 ) == ">" ){
			// the name of the scaffold
			m_scafName = line.substr( 1 );
		}
		else{
			// save each contig
			vector<string> *contents = new vector<string>;
			Split( line, "\t", contents );
			Contig *newContig = new Contig();
			newContig->SetName( (*contents)[ 0 ] );
			newContig->SetID( num );
			newContig->SetScaffoldPos( num );
			newContig->SetLength( atoi( (*contents)[ 2 ].c_str() ) );
			if( (*contents)[ 1 ] == "BE" )
				newContig->SetOri( PLUS );
			else
				newContig->SetOri( MINUS );
			
			num++;
			contents->clear();

			m_contigs->push_back( newContig );
			m_contigsName->insert( pair<string, Contig*> ( (*contents)[ 0 ], newContig ) );
			delete contents;
		}
	}
	

	scafReader.close();
	return 1;
}

int GapEstimater::ReadClusters( string clusterFile ){
	ifstream clusterReader( clusterFile.c_str() );
	
	if( clusterReader == NULL ){
		cout<<"Cannot open "<<clusterFile<<" file\n";
		return -1;
	}

	string line = "";
	
	int num = 0;
	while( getline( clusterReader, line ) ){
		vector<string> *contents = new vector<string>;
		Split( line, "\t", contents );
		Contig *startContig;
		Contig *endContig;
		if( m_contigsName->find( contents->at( 0 ) ) != m_contigsName->end() )
			startContig = (*m_contigsName)[ contents->at( 0 ) ];
		else
			continue;

		if( m_contigsName->find( contents->at( 2 ) ) != m_contigsName->end() ) 
			endContig = (*m_contigsName)[ contents->at( 2 ) ];
		else
			continue;
		
		int startContigOri = PLUS;
		if( contents->at( 1 ) == "-" )
			startContigOri = MINUS;

		int endContigOri = PLUS;
		if( contents->at( 3 ) == "-" )
			endContigOri = MINUS;

		int dis = atoi( contents->at( 4 ).c_str() );
		int std = atoi( contents->at( 5 ).c_str() );
		int size = atoi( contents->at( 6 ).c_str() );

		PET *newEdge = new PET( startContig, startContigOri, endContig, endContigOri, dis, std, size );
		m_edges->push_back( newEdge );
		startContig->AddEdge( newEdge );
		endContig->AddEdge( newEdge );
		
		contents->clear();
		delete contents;
	}
	clusterReader.close();
	return 1;
}

// calculate the gap sizes
void GapEstimater::CalculateGap( int kmer ){
	int numberOfGaps = m_contigs->size() - 1;

	// initialize gap sizes
	Matrix<double> G, CE, CI;
	Vector<double> g0, ce0, ci0, x;
	//int n, m, p;
	double sum = 0.0;

	G.resize( numberOfGaps, numberOfGaps );
	g0.resize( numberOfGaps );
	for (int i = 0; i < numberOfGaps; i++){
		for (int j = 0; j < numberOfGaps; j++)
			G[i][j] = 0;
		g0[ i ] = 0;
	}

	CE.resize( numberOfGaps, 0 );
	CI.resize( numberOfGaps, 0 );
	ce0.resize( 0 );
	ci0.resize( 0 );
	x.resize( numberOfGaps );

	// traverse each contig
#ifdef LOG
	//fprintf( logFile, "\nnew scaffolds\n" );
#endif

	string constraintsStr = ""; 
	int numberOfEdges = 0;      // the number of all concordant edges
	set<PET*> *visitedEdges = new set<PET*>;
	for( vector<Contig*>::iterator contigIter = m_contigs->begin(); contigIter!= m_contigs->end(); contigIter++ ){
		Contig *contig = *contigIter;
#ifdef LOG
		//fprintf( logFile, "%s\t%d\t%.0f\n", contig->GetName().c_str(), contig->GetOri(), contig->GetLength() );
#endif
		// update parameters
		list<PET*> *edges = new list<PET*>;
		edges->insert( edges->begin(), contig->GetEdges()->begin(), contig->GetEdges()->end() );

		for( list<PET*>::iterator edgeIter = edges->begin(); edgeIter != edges->end(); edgeIter++ ){
			PET *edge = *edgeIter;

			// check if visited this edge before
			if( visitedEdges->find( edge ) == visitedEdges->end() )
				visitedEdges->insert( edge );
			else
				continue;

			numberOfEdges++;

			int firstPos = edge->GetStartContig()->GetScaffoldPos();
			int secondPos = edge->GetEndContig()->GetScaffoldPos();
			//cout<<firstPos<<"\t"<<secondPos<<endl;
			if( firstPos > secondPos ){
				int temp = secondPos;
				secondPos = firstPos;
				firstPos = temp;
			}

			double sumLength = 0;		// the total contig length in the middle
			for( int id = firstPos + 1; id < secondPos; id++ ){
				sumLength += m_contigs->at( id )->GetLength();
			}
			constraintsStr += itos( firstPos ) + "\t" + itos( secondPos ) + "\t" 
				+ itos( edge->GetDis() + 6 * edge->GetStd() - sumLength ) + "\n";


			// update related gap coefficients
			for( int gapID = firstPos; gapID < secondPos; gapID++ ){
				g0[ gapID ] += 2.0 * ( sumLength - (double) edge->GetDis() ) 
					/ ( (double)edge->GetStd() * (double)edge->GetStd() );
				//g0[ gapID ] += 2.0 * ( sumLength - (double) edge->GetDis() ) 
				//	/ ( (double)edge->GetStd() * (double)edge->GetStd() );
				G[ gapID ][ gapID ] += 2.0 / (double)( edge->GetStd() * edge->GetStd() );
				for( int gapID2 = firstPos; gapID2 < secondPos; gapID2++ )
				{
					if( gapID != gapID2 )
						G[ gapID ][ gapID2 ] += 2.0 / (double)( edge->GetStd() * edge->GetStd() );
				}
			}
		}

		delete edges;
	}
	visitedEdges->clear();
	delete visitedEdges;

	/*cout<<"G:\n";
	for( int i = 0; i < numberOfGaps; i++ ){
		for( int j = 0; j < numberOfGaps; j++ )
			cout<<G[ i ][ j ]<<"\t";
		cout<<endl;
	}
	cout<<endl;

	cout<<"g0:\n";
	for( int i = 0; i < numberOfGaps; i++ ){
		cout<<g0[ i ]<<"\t";
	}
	cout<<endl;
	*/

	// add constraints: gi>=-kmer
	CI.resize( numberOfGaps, numberOfGaps + numberOfEdges );
	ci0.resize( numberOfGaps + numberOfEdges );
	for( int i = 0; i < numberOfGaps; i++ ){
		ci0[ i ] = kmer;
		for( int j = 0; j < numberOfGaps; j++ )
		{
			if( i == j )
				CI[ i ][ j ] = 1;
			else
				CI[ i ][ j ] = 0;
		}
	}

	vector<string>* cons = new vector<string>;
	Split( constraintsStr, "\n", cons );
	for( int i = numberOfGaps; i < numberOfGaps + numberOfEdges; i++ ){
		vector<string>* line = new vector<string>;
		Split( cons->at( i - numberOfGaps ), "\t", line );
		int start = atoi( line->at( 0 ).c_str() );
		int end = atoi( line->at( 1 ).c_str() );
		int value = atoi( line->at( 2 ).c_str() );
		ci0[ i ] = value;
		for( int j = 0; j < numberOfGaps; j++ )
		{
			if( j >= start && j < end )
				CI[ j ][ i ] = -1;
			else
				CI[ j ][ i ] = 0;
		}
		line->clear();
		delete line;
	}
	cons->clear();
	delete cons;
		
	m_resultValue = solve_quadprog(G, g0, CE, ce0, CI, ci0, x);

	// set the gap sizes to contigs
	for( int id = 0; id < m_contigs->size() - 1; id++ ){
		if( (int)x[ id ] >= -kmer )
			m_contigs->at( id )->SetGap( (int)x[ id ] );
		else
			m_contigs->at( id )->SetGap( -kmer );
	}
	m_contigs->at( m_contigs->size() - 1 )->SetGap( 0 );
}

int GapEstimater::OutputScaffolds( string outputFile ){
	ofstream scafWriter( outputFile.c_str() );
	if( scafWriter == NULL ){
		cout<<"Cannot open "<<outputFile<<"\n";
		return -1;
	}

	char buffer[ 200 ];
	sprintf( buffer, ">%s quadValue %.2f\n", m_scafName.c_str(), m_resultValue ); 
	//string results = ">" + m_scafName + " quadValue " + itos( m_resultValue ) + "\n";
	string results( buffer );
	for( int id = 0; id < m_contigs->size(); id++ ){
		Contig *c = m_contigs->at( id );
		results += c->GetName() + "\t";
		if( c->GetOri() == PLUS )
			results += "BE\t";
		else
			results += "EB\t";

		results += itos( c->GetLength() ) + "\t" + itos( c->GetGap() ) + "\n";
	}

	scafWriter.write( results.c_str(), results.length() );
	scafWriter.close();

	return 1;
}

// estimate the gap sizes
int main(int argc, char *argv[] )
{
	if( argc == 1 || string( argv[ 1 ] ) == "-h" || argc != 5 ){
		cout<<"Usage of gapEstimater:\n";
		cout<<"bin/gapEstimater <scaffold-file> <cluster-file> <output-file> <kmer>\n";
		return -1;
	}

	string scafFile = string( argv[ 1 ] );
	string clusterFile = string( argv[ 2 ] );
	string outputFile = string( argv[ 3 ] );
	int kmer = atoi( argv[ 4 ] );

	GapEstimater *myEstimater = new GapEstimater();

	// read scaffolds file to save all contigs
	cout<<"Reading scaffold file...\n";
	if( myEstimater->ReadScaffolds( scafFile ) == -1 ){
		cout<<"Reading scaffold file unsuccessfully\n";
		return -1;
	}

	// read cluster file to save edges
	cout<<"Reading cluster file...\n";
	if( myEstimater->ReadClusters( clusterFile ) == -1 ){
		cout<<"Reading cluster file unsuccessfully\n";
		return -1;
	}
	
	// calculate gaps
	cout<<"Calculating gaps...\n";
	myEstimater->CalculateGap( kmer );

	// output scaffolds
	cout<<"Outputing results...\n";
	if( myEstimater->OutputScaffolds( outputFile ) == -1 ){
		cout<<"Cannot output scaffolds file\n";
		return -1;
	}

	delete myEstimater;
	return 1;
}
