#ifndef LOCAL_SEARCH_H
#define LOCAL_SEARCH_H

#define NO_EDGE_CONSTANT -10000
#define MINUS_MINUS 0
#define MINUS_PLUS 1
#define PLUS_MINUS 2
#define PLUS_PLUS 3
#define EB 0
#define BE 1

#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <map>
#include <unordered_map>
#include <queue> 
#include <boost/bimap.hpp>
#include <cmath>

#include "loader.h"
#include "scaffold_graph.h"


using namespace std;

/*
 * OpenNodesInnerQueue - class for storing opened Contig objects.
 * It is used for inner BFS (graph connectivity checking)
 * VisitedContigsNodes - class for storing visited nodes - Contig 
 * objects. This class has its own compare function for visitedContigs
 * map.
*/

class OpenNodesInnerQueue{
	public:
	queue<Contig *> openNodesInner;
};

class VisitedNodesInnerMap{
	public:
	struct classcomp{
		bool operator () (Contig * contig1, Contig * contig2){
			if ((contig1 -> name) > (contig2 -> name))
				return true;
			else
				return false;
		}
	};
	map<Contig *, int, classcomp> visitedContigs;
	map<Contig *, int, classcomp>::iterator it;
};

/*
 * OpenNodesQueue - similar as queue structure above.
 * Used for outer gready search method. Partial scaffold
 * is considered to be the node in this case.
 * */

class OpenNodesQueue{
	public:
	queue<SubScaffold *> openNodes;
};

/*
 * Filter - class for the basic edge filtering. 
 * Here we filter edges that are connecting different
 * scaffolds or partial scaffolds. It also contains 
 * filterMap (map) strucutre used to link contig
 * names with pointer to Contig objects.
 * */

class Filter{
	public:
	unordered_map<string, Contig *> filterMap;
	void constructFilterMap(vector<Scaffold *> * allScaffolds){
		long int numberOfScaffolds = (*allScaffolds).size();
		for (long int i = 0; i < numberOfScaffolds; i++){
			Scaffold * currentScaff = (*allScaffolds)[i];
			for (long int j = 0; j < currentScaff->scaffold.size(); j++){
				filterMap.insert(make_pair(currentScaff->scaffold[j]->name, currentScaff->scaffold[j]));
			}
		}
	}
	
	Contig * getPointerToContigObject(string contigName){
		return filterMap[contigName];
	}
	
	vector<vector<string> > filterScaffoldConnectingEdges(vector<vector<string> > edgeData){
		unordered_map<string, Contig *>::iterator filterIterator1, filterIterator2;
		vector<vector<string> > validEdgeData;
		long int validEdges = 0;
		cout << "Filtering edges which are connecting different scaffolds..." << endl;
		//cout << "ima ih: " << edgeData.size() << endl;
		cout.flush();
		for (long int i = 0; i < edgeData.size(); i++){
			string contigName1 = edgeData[i][0];
			string contigName2 = edgeData[i][2];
			//cout << "prvi drugi" << contigName1 << " " << contigName2 << endl;
			filterIterator1 = filterMap.find(contigName1);
			filterIterator2 = filterMap.find(contigName2);
			if ((filterIterator1 != filterMap.end()) && (filterIterator2 != filterMap.end()))
				if ((filterIterator1->second->scaffoldID) == (filterIterator2->second->scaffoldID)){
					validEdgeData.push_back(edgeData[i]);
					validEdges++;
				}	
		
		}
		cout << "Done." << endl;
		return validEdgeData;
	}
	
	vector<vector<string> > filterSubscaffoldConnectingEdges(vector<vector<string> > edgeData){
		unordered_map<string, Contig *>::iterator filterIterator1, filterIterator2;
		vector<vector<string> > validEdgeData;
		cout << "Filtering edges which are connecting different partial scaffolds..." << endl;
		long int validEdges = 0;
		for (long int i = 0; i < edgeData.size(); i++){
			string contigName1 = edgeData[i][0];
			string contigName2 = edgeData[i][2];
			filterIterator1 = filterMap.find(contigName1);
			filterIterator2 = filterMap.find(contigName2);
			if ((filterIterator1 != filterMap.end()) && (filterIterator2 != filterMap.end()))  
				if ((filterIterator1->second->subscaffoldID) == (filterIterator2->second->subscaffoldID)){
					validEdgeData.push_back(edgeData[i]);
					validEdges++;
				}
			else if (abs((filterIterator1->second->subscaffoldID) - (filterIterator2->second->subscaffoldID)) == 1){
				if ((filterIterator1->second->border) ^ (filterIterator2->second->border)){
					validEdgeData.push_back(edgeData[i]);
					validEdges++;
				}
			}
		
		}
		cout << "Done." << endl;
		return validEdgeData;
	}
	
	void printValidEdges(vector<vector<string> > edgeData){
		cout << "Valid edges set: " << endl;
		for (long int i = 0; i < edgeData.size(); i++){
			cout << "\t" << edgeData[i][0] << "\t" << edgeData[i][1] << "\t" << edgeData[i][2] << "\t" << edgeData[i][3] << "\t" << edgeData[i][4] << "\t" << edgeData[i][5] << endl;
		}
	}
};

/*
 * BFSSearch - class used to check Scaffold graph connectivity
 * before removing some of its nodes. It implements simple BFS
 * from one side of partial scaffold to another. If the end is
 * reachable and all of visible inner contigs were visited during
 * the serach then the goalFunction returns true so we can proceed
 * with Contig removal.
 * */

class BFSSearch{
	public:
	OpenNodesInnerQueue * openContigNodes;
	VisitedNodesInnerMap * visitedContigNodes;
        Contig * beginSearchContig;
        Contig * endSearchContig;
	ofstream * statsFile;
	int visibleNodes;
	int connectedNodes;
	int partialSize;
	
	Contig * removeHead(OpenNodesInnerQueue * queue){
		Contig * oldest  = queue -> openNodesInner.front();
		queue->openNodesInner.pop();
		return oldest;
	}
	
	void insertBack(Contig * child, OpenNodesInnerQueue * queue){
		queue->openNodesInner.push(child);
	}
	
	void makeVisited(Contig * contig, VisitedNodesInnerMap * visitedContigNodes, int componentNum){
		visitedContigNodes->visitedContigs[contig] = componentNum;
	}
	
	bool checkIfVisited(Contig * contig, VisitedNodesInnerMap * visitedContigNodes){
		if (visitedContigNodes->visitedContigs.find(contig) == visitedContigNodes -> visitedContigs.end())
			return false;
		return true;
	}
	
	void expandCurrentContig(Contig * currentContig, OpenNodesInnerQueue * queue, VisitedNodesInnerMap * visitedContigNodes, ScaffoldGraph * scaffoldGraph, Filter * dataLinker){
		vector<string> linkedContigs;
		makeVisited(currentContig, visitedContigNodes, 1);
		linkedContigs = scaffoldGraph -> getContigPairs(currentContig -> name);
		for (int i = 0; i < linkedContigs.size(); i++){
			Contig * linkedContig = dataLinker -> getPointerToContigObject(linkedContigs[i]);
			if ((linkedContig -> isVisible()) && (linkedContig -> isDeleted() == false)){
				if(!checkIfVisited(linkedContig, visitedContigNodes)){
					insertBack(linkedContig, queue);
				}
			}  	
		} 
		
		//makeVisited(currentContig, visitedContigNodes, 1);
	}
	
	bool goalFunction(Contig * contig, VisitedNodesInnerMap * visitedContigNodes, SubScaffold * partialScaffold, ofstream * bfsStats){
		if (checkIfVisited(endSearchContig, visitedContigNodes)){
			if (partialSize == visitedContigNodes -> visitedContigs.size()){
				return true;	
			}
		}
		return false;
	}

	Contig * getSearchBegin(SubScaffold * partialScaffold){
		for (int i = 0; i < partialScaffold -> innerContigs.size(); i++){
		if ((partialScaffold -> innerContigs[i] -> isVisible()) && (partialScaffold -> innerContigs[i] -> isDeleted() == false))
				return partialScaffold -> innerContigs[i]; 
		}
	}

	Contig * getSearchEnd(SubScaffold * partialScaffold){
		for (int i = partialScaffold -> innerContigs.size() -1; i >= 0; i--){
		if ((partialScaffold -> innerContigs[i] -> isVisible()) && (partialScaffold -> innerContigs[i] -> isDeleted() == false))
                                return partialScaffold -> innerContigs[i];
                }
	}

	void makePartialScaffoldVisible(SubScaffold * partialScaffold){
		if (partialScaffold -> borderContig != NULL)
			partialScaffold -> borderContig -> setVisible();
		for (int i = 0; i < partialScaffold -> innerContigs.size(); i++)
			partialScaffold -> innerContigs[i] -> setVisible();
		if (partialScaffold -> borderContigEnd != NULL)
			partialScaffold -> borderContigEnd -> setVisible();
	}

	void makePartialScaffoldInvisible(SubScaffold * partialScaffold){
		if (partialScaffold -> borderContig != NULL)
                        partialScaffold -> borderContig -> setInvisible();
                for (int i = 0; i < partialScaffold -> innerContigs.size(); i++)
                        partialScaffold -> innerContigs[i] -> setInvisible();
                if (partialScaffold -> borderContigEnd != NULL)
                        partialScaffold -> borderContigEnd -> setInvisible();
	}
	
	int graphSearch(SubScaffold * partialScaffold, Filter * dataLinker, ScaffoldGraph * scaffoldGraph, ofstream * bfsStats){
		
		openContigNodes = new OpenNodesInnerQueue();
		visitedContigNodes = new VisitedNodesInnerMap();
		int innerSize = partialScaffold -> innerContigs.size();
		beginSearchContig = NULL;
		endSearchContig = NULL;
		visibleNodes = 0;
		connectedNodes = 0;
		partialSize = 0;
		//makePartialScaffoldVisible(partialScaffold);
		(*bfsStats) << "Starting BFS..." << endl;	
		if (partialScaffold -> borderContig != NULL){
			beginSearchContig = partialScaffold -> borderContig;
			partialSize++;
		}else{
			beginSearchContig = getSearchBegin(partialScaffold);
		}
		(*bfsStats) << "Starting point: " << beginSearchContig -> name << endl;
		if (partialScaffold -> borderContigEnd != NULL){
			endSearchContig = partialScaffold -> borderContigEnd;
			partialSize++;
		}else{
			endSearchContig = getSearchEnd(partialScaffold);
		}
		partialSize += partialScaffold -> innerContigs.size() - 1;
		(*bfsStats) << "Ending point: " << endSearchContig -> name << endl;		
		insertBack(beginSearchContig, openContigNodes);
		while (!(openContigNodes->openNodesInner.empty())){
			Contig * currentContig = removeHead(openContigNodes);
			if (!(checkIfVisited(currentContig, visitedContigNodes))){
				 expandCurrentContig(currentContig, openContigNodes, visitedContigNodes, scaffoldGraph, dataLinker);
			}
			//makeVisited(currentContig, visitedContigNodes, 1);
			if (goalFunction(currentContig, visitedContigNodes, partialScaffold, bfsStats)){
				(*bfsStats) << "YES" << "\t" << "YES" << "\t" << visitedContigNodes -> visitedContigs.size() << "\t" << partialSize << endl;
				delete openContigNodes;
				delete visitedContigNodes;
				return 1;
			}
		}

		if(checkIfVisited(endSearchContig, visitedContigNodes)){
			 (*bfsStats) << "NO" << "\t" << "YES" << "\t" << visitedContigNodes -> visitedContigs.size() << "\t" <<
partialSize << endl;

		}else{
			 (*bfsStats) << "NO" << "\t" << "NO" << "\t" << visitedContigNodes -> visitedContigs.size() << "\t" <<
partialSize << endl;

		}
		(*bfsStats) << "Visible nodes: " << partialSize  << endl;
		(*bfsStats) << endl;
		delete openContigNodes;
		delete visitedContigNodes;
		return 0;
	}
	
};

/*
 * DistanceMatrix - simple class used for storing distances between
 * each pair of contigs inside the partial scaffold.
 * It also contains update and reverse function that enable changing
 * certain values of matrix in the linear time.
 * */

class DistanceMatrix{
	public:
	unordered_map<Contig *, int> matrixPositionsMapper;
	unordered_map<Contig *, int>::iterator it;
	int sX, sY;
	int *matrix;
	
	void mapPositions(SubScaffold * sub){
		int innerSize = sub->innerContigs.size();
		matrixPositionsMapper.insert(make_pair(sub->borderContig, 0));
		(sub->borderContig)->assignMatrixId(0);
		for (int i = 0; i < innerSize; i++){
			matrixPositionsMapper.insert(make_pair(sub->innerContigs[i], i+1));
			(sub->innerContigs[i])->assignMatrixId(i+1);
		}
		matrixPositionsMapper.insert(make_pair(sub->borderContigEnd, innerSize+1));
		(sub->borderContigEnd)->assignMatrixId(innerSize + 1);
	}
	
	int getPositionByContig(Contig * contig){
		return matrixPositionsMapper[contig];
	}
	
	int getFieldValue(int i, int j){
		return matrix[i * sX + j];
	}
	
	int getDimension(){
		return sX;
	}
	
	int getDistance(Contig * contig1, Contig * contig2){
		int indexOfFirst = getPositionByContig(contig1);
		int indexOfSecond = getPositionByContig(contig2);
		return matrix[indexOfFirst * sY + indexOfSecond];
	}
	
	void fillMatrix(SubScaffold * sub){
		int dimension = sub->innerContigs.size();
		int rowSum = 0;
		int rowSumEnd = 0;
		for (int i = 0; i < sX - 1; i++)
		{
			matrix[i + 1] = rowSum;
			if (i < dimension){
			
			rowSum += sub->innerContigs[i]->length;
			}
		}
		for (int i = 0; i < dimension; i++){
			int rowSum = 0;
			for (int j = i + 1; j < dimension; j++){
				matrix[(i + 1) * sY + j + 1] = rowSum;
				rowSum += sub->innerContigs[j]->length;
				if (j == (dimension -1)){
					matrix[(i + 1) * sY + j + 2] = rowSum;
				}
			}
		}
	}
	
	int getPositionOfContig(Contig * contig){
		return contig->matrixId;
	}
	
	void printAllMappingInfo(SubScaffold * sub){
		cout << sub->borderContig->name << " : " << getPositionOfContig(sub->borderContig) << endl;
		for (int i = 0; i < sub->innerContigs.size(); i++){
			cout << sub->innerContigs[i]->name << " : " << getPositionOfContig(sub->innerContigs[i]) << endl;
		}
		cout << sub->borderContigEnd->name << " : " << getPositionOfContig(sub->borderContigEnd) << endl;
	}
	
	void updateMatrix(int firstPos, int secondPos, Contig * firstContig, Contig * secondContig){ //secondPos > firstPos
		firstPos++;
		secondPos++;
		for (int i = 0; i < firstPos; i++){
			matrix[i * sX + firstPos] += secondContig->length;
			matrix[i * sX + secondPos] -= firstContig->length;
		}
		for (int i = secondPos + 1; i < sX; i++){
			matrix[sX * firstPos + i] -= secondContig->length;
			matrix[sX * secondPos + i] += firstContig->length;
		}
	}
	
	void updateMatrixReverse(int firstPos, int secondPos, Contig * firstContig, Contig * secondContig){
		firstPos++;
		secondPos++;
		for (int i = 0; i < firstPos; i++){
			matrix[i * sX + firstPos] -= secondContig->length;
			matrix[i * sX + secondPos] += firstContig->length;
		}
		for (int i = secondPos + 1; i < sX; i++){
			matrix[sX * firstPos + i] += secondContig->length;
			matrix[sX * secondPos + i] -= firstContig->length;
		}
	}
	
	DistanceMatrix(int sizeX, int sizeY){
		sX = sizeX;
		sY = sizeY;
		matrix = new int[sX*sY];
		for (int i = 0; i < sX; i++){
			for (int j = 0; j < sY; j++){
				matrix[i * sY + j] = 0;
			}
		}
	}
	
	~DistanceMatrix(){
		delete [] matrix;
	}
	
	void printDistanceMatrix(){
		for (int i = 0; i < sX; i++){
			for (int j = 0; j < sY; j++){
				cout << matrix[i * sY + j] << "\t";
			}
			cout << endl;
		}
	}
	
};

/*
 * FixedMatrix - class for storing methods that are available
 * inside all other fixed matrix structures (like an edgeMatrix)
 * - mapper for matrix header.
 * */

class FixedMatrix{
	public:
		unordered_map<string, int> matrixHeader;
		int sX;
		
	void defineMatrixHeader(SubScaffold * partialScaffold){
		int headerIndex = 0;
		if (partialScaffold->borderContig != NULL){
			partialScaffold->borderContig->assignEdgeMatrixId(headerIndex);
			matrixHeader.insert(make_pair(partialScaffold->borderContig->name, headerIndex));
		}
		headerIndex++;
		for (int i = 0; i < partialScaffold->innerContigs.size(); i++){
			partialScaffold->innerContigs[i]->assignEdgeMatrixId(headerIndex);
			matrixHeader.insert(make_pair(partialScaffold->innerContigs[i]->name, headerIndex));
			headerIndex++;
		}
		if (partialScaffold->borderContigEnd != NULL){
			partialScaffold->borderContigEnd->assignEdgeMatrixId(headerIndex);
			matrixHeader.insert(make_pair(partialScaffold->borderContigEnd->name, headerIndex));
		}
	}
	
	int getHeaderIdInitial(string contigName){
		return matrixHeader[contigName];
	}
};

/*
 * EdgeMatrix - class used for storing info about the edges. 
 * If there is no edge between the two contigs, NO_EDGE_CONSTANT
 * value is put in corresponding field. We compare this structure
 * with the distanceMatrix at each step to detect
 * happy edge criteria violation.
 * */

class EdgeDataMatrix: public FixedMatrix{
	public:

	int * edgeMatrix;
	Filter * objectLinker;
        ofstream tempEdgeFile;
        ofstream tempScaffoldFile;


	
	EdgeDataMatrix(SubScaffold * partialScaffold, Filter * filter){
		int index = 0;
		sX = partialScaffold -> innerContigs.size() + 2;
		edgeMatrix = new int[sX*sX];
		for (int i = 0; i < sX; i++)
			for (int j = 0; j < sX; j++){
				edgeMatrix[i * sX + j] = NO_EDGE_CONSTANT;
			}
		objectLinker = filter;
		defineMatrixHeader(partialScaffold);
	}
	
	~EdgeDataMatrix(){
		delete[] edgeMatrix;
	}
	
	int getHeaderId(Contig * contig){
		return contig->getEdgeMatrixId();
	}
	
	int calculateSum(int mean, int stDev){
		return mean + 6 * stDev;
	}
	
	void updateMatrixField(int i, int j, int value){
		edgeMatrix[i * sX + j] = value;
	}
	
	int getFieldValue(int i, int j){
		return edgeMatrix[i * sX + j];
	}
	
	int reverseOrientation(unsigned int orientation){
		orientation = orientation ^ 3;
		return orientation;
	}
	
	int convertOrientationToInt(string o){
		if (o == "BEBE")
			return 3;
		else if (o == "BEEB")
			return 2;
		else if (o == "EBBE")
			return 1;
		else if (o == "EBEB")
			return 0;
		return -1;
	}

	char transformOrientation(string o){
		if (o == "BE")
			return '+';
		return '-';
	}
	
	bool checkOrientation(Contig * contig1, Contig * contig2, int orientationInEdge){
		int orientationInScaffold = convertOrientationToInt(contig1->orientation + contig2->orientation);
		if ((orientationInScaffold == orientationInEdge) || (orientationInScaffold == reverseOrientation(orientationInEdge)))
			return true;
	}
	
	void calculateMatrixValues(vector<string> pairedContigs, ScaffoldGraph * scaffoldGraph, Contig * contig){
		int headerIndex1 = getHeaderId(contig);
		for (int i = 0; i < pairedContigs.size(); i++){
			int mean = scaffoldGraph -> getEdgeData(contig->name, pairedContigs[i]);
			int stDev = scaffoldGraph -> getEdgeData(contig->name, pairedContigs[i],1);
			int readNum = scaffoldGraph -> getEdgeData(contig->name, pairedContigs[i],2);
			int sum = calculateSum(mean, stDev);
			int headerIndex2 = getHeaderIdInitial(pairedContigs[i]);
			Contig * secondContig = objectLinker -> getPointerToContigObject(pairedContigs[i]);
			if (contig->id < secondContig->id)
			tempEdgeFile <<  contig->name << "\t" << transformOrientation(contig->orientation) << "\t" << secondContig -> name << "\t" << transformOrientation(secondContig -> orientation)  << "\t" << mean << "\t" << stDev << "\t" << readNum << endl;
			updateMatrixField(headerIndex1, headerIndex2, sum);
		}
	}	
	
	void fillEdgeMatrix(SubScaffold * partialScaffold, ScaffoldGraph * scaffoldGraph){
		vector<string> pairedContigs;
		tempEdgeFile.open("src/partial_edges.dat");
		if (partialScaffold->borderContig != NULL){
			if (scaffoldGraph->checkIfContigIsVertex(partialScaffold->borderContig->name)){
				int headerPos =  getHeaderId(partialScaffold->borderContig);
				pairedContigs = scaffoldGraph -> getContigPairs(partialScaffold->borderContig->name);
				calculateMatrixValues(pairedContigs, scaffoldGraph,partialScaffold->borderContig);
			}
			//tempScaffoldFile << partialScaffold->borderContig->name<<"\t"<<partialScaffold->borderContig->orientation<<"\t"<<partialScaffold->borderContig->length<<"\t"<<partialScaffold->borderContig->rightGap<<endl;
		}
		for (int i = 0; i < partialScaffold->innerContigs.size(); i++){
			if (scaffoldGraph->checkIfContigIsVertex(partialScaffold->innerContigs[i]->name)){
				int headerPos =  getHeaderId(partialScaffold->innerContigs[i]);
				pairedContigs = scaffoldGraph -> getContigPairs(partialScaffold->innerContigs[i]->name);
				calculateMatrixValues(pairedContigs, scaffoldGraph,partialScaffold->innerContigs[i]);
			}
			//tempScaffoldFile << partialScaffold->innerContigs[i]->name<<"\t"<<partialScaffold->innerContigs[i]->orientation<<"\t"<<partialScaffold->innerContigs[i]->length<<"\t"<<partialScaffold->innerContigs[i]->rightGap<<endl;

		}
		if (partialScaffold->borderContigEnd != NULL){
			if (scaffoldGraph->checkIfContigIsVertex(partialScaffold->borderContigEnd->name)){
				int headerPos =  getHeaderId(partialScaffold->borderContigEnd);
				pairedContigs = scaffoldGraph -> getContigPairs(partialScaffold->borderContigEnd->name);
				calculateMatrixValues(pairedContigs, scaffoldGraph,partialScaffold->borderContigEnd);
			}
			//tempScaffoldFile << partialScaffold->borderContigEnd->name<<"\t"<<partialScaffold->borderContigEnd->orientation<<"\t"<<partialScaffold->borderContigEnd->length<<"\t"<<partialScaffold->borderContigEnd->rightGap<<endl;

		}
		tempEdgeFile.close();
		//tempScaffoldFile.close();
			
	}
	
	void printEdgeMatrix(){
		for (int i = 0; i < sX; i++){
			for (int j = 0; j < sX; j++)
			cout << edgeMatrix[i * sX + j] << "\t";
			cout << endl;
		}
	}
};

/*
 * VisitedNodesMap - structure used to check which partial
 * scaffolds have already been processed by local search.
 * */

class VisitedNodesMap{
	public:
	struct classcomp{
        bool operator () (const SubScaffold * s1, const SubScaffold * s2){
            for (int i = 0; i < s1 -> innerContigs.size(); i++){
                if (((s1->innerContigs[i])->name) > ((s2->innerContigs[i])->name))
                       return true;
                else if (((s1->innerContigs[i])->name) < ((s2->innerContigs[i])->name))
                       return false;
            }
            return false;
        }
    };
	map<SubScaffold *, int, classcomp> visitedNodes;
	map<SubScaffold *, int, classcomp>::iterator it;
};

/*
 * LocalSearch - class which implements local search between
 * partial scaffolds. In every step of local search, current
 * partial scaffold is expanded - expand function.
 * Every child is created by swapping two adjacent contigs.
 * When we expand the node, we put it in the visitedNodes
 * structure in order not to process it again. We do local
 * search on each partial scaffold separately by iterating
 * trought the vector of partial scaffolds which is the
 * member of the Scaffold structure. After every swap we need
 * to check the new partial scaffold goodness - happy
 * edge criteria. If criteria is not violated - swap is
 * accepted and contig removed if removal doesn't
 * brake the scaffoldGraph connectivity.
 * */

class LocalSearch{
	public:
	OpenNodesQueue * openPartialScaffolds;
	VisitedNodesMap * visitedPartialScaffolds;
	map<string, vector<string> > contigData;
	vector<vector<string> > edgeData;
	ScaffoldGraph * scaffoldGraph;
	EdgeLoader * edge_loader;
	Loader * loader;
	Loader * data;
	Filter * filter;
	int unvalidEdges;
	int numberOfAmbiguousRegions;
	int changed;
	int numberOfInitialUnhappyEdges;
	bool partialScaffoldChanged;
	bool errorDetected;
	ofstream * bfsStats;
	ofstream statsFile;
	
	SubScaffold * removeHead(OpenNodesQueue * queue){
		SubScaffold * oldest = queue->openNodes.front();
		queue->openNodes.pop();
		return oldest;
	}	
	
	void insertBack(SubScaffold * child, OpenNodesQueue * queue){
		queue->openNodes.push(child);
	}
	
	bool goalFunction(SubScaffold * partialScaffold){
		return false;
	}
	
	void makeVisited(SubScaffold * partialScaffold, VisitedNodesMap * visitedPartialScaffolds){
		visitedPartialScaffolds->visitedNodes[partialScaffold] = partialScaffold->depth;
	}
	
	bool checkIfVisited(SubScaffold * partialScaffold, VisitedNodesMap * visitedPartialScaffolds){
		if (visitedPartialScaffolds->visitedNodes.find(partialScaffold) == visitedPartialScaffolds->visitedNodes.end())
			return false;
		return true;
	}
	
	void checkInitialPartialScaffoldGoodnes(SubScaffold * partialScaffold){
		int sX = partialScaffold -> getSizeOfInnerContigs() + 2;
		EdgeDataMatrix * edgeMatrix = new EdgeDataMatrix(partialScaffold, filter);
		DistanceMatrix * distanceMatrix = new DistanceMatrix(sX, sX);
		distanceMatrix -> fillMatrix(partialScaffold);
		for (int i = 0; i < sX; i++){
			for (int j = i + 1; j < sX; j++){
				if (edgeMatrix -> getFieldValue(i, j) != NO_EDGE_CONSTANT)
					if (distanceMatrix -> getFieldValue(i, j) >= edgeMatrix -> getFieldValue(i, j)){
						partialScaffold -> incrementNumberOfUnhappyEdges();
						numberOfInitialUnhappyEdges++;
					}
			}
		}
		delete edgeMatrix;
		delete distanceMatrix;
	}
	
	void iterateOverPartialScaffolds(){
		cout << "Rescaffolding..." << endl;
		for (long int i = 0; i < data -> scaffolds.size(); i++){
			Scaffold * currentScaffold = data -> scaffolds[i];
			vector<SubScaffold *> pom = currentScaffold ->partialScaffold;
			cout << "Processing scaffold: " << data->scaffolds[i]->id + 2 << endl;
			if (pom.empty()){
				SubScaffold * imaginarySub = new SubScaffold();
				vector <Contig*> pomInnerCon = currentScaffold->scaffold;
				imaginarySub->innerContigs = pomInnerCon;
				if (pomInnerCon.size() >= 2){	
					checkInitialPartialScaffoldGoodnes(imaginarySub);
					changed = false;
					do{
					doBFS(imaginarySub);
					}while(errorDetected);
					if (changed)
						makeSystemCall(imaginarySub);

				}
				delete imaginarySub;
					
			}
			else
			for (int j = 0; j < pom.size(); j++){
				if ((pom[j] -> innerContigs.size()) >= 2){
					checkInitialPartialScaffoldGoodnes(pom[j]);
					changed = false;
					do{
					doBFS(pom[j]);
					}while(errorDetected);
					if (changed)
						makeSystemCall(pom[j]);

				}
					
			}
			cout << "Done." << endl;
		}
	}
	
	void resetGapSize(Contig * removedContig){
		removedContig->rightGap = 0;
	}
	
	bool checkOrientationBrackage(EdgeDataMatrix * edgeMatrix, Contig * firstContig, Contig * secondContig){
		int index1 = firstContig -> getEdgeMatrixId();
		int index2 = secondContig -> getEdgeMatrixId();
		int edgeMatrixValue = edgeMatrix -> getFieldValue(index1, index2);
		if (edgeMatrixValue != NO_EDGE_CONSTANT)
			return true;
		return false;
	}
	
	bool checkValidityNew(SubScaffold * partialScaffold, DistanceMatrix * distanceMatrix, EdgeDataMatrix * edgeMatrix, int firstPos, 
							int secondPos, Contig * firstContig, Contig * secondContig){ //secondPos > firstPos
		int numberOfUnhappyEdges = 0;
		if (checkOrientationBrackage(edgeMatrix, firstContig, secondContig))
			return false;
		firstPos++;
		secondPos++;
		int sX = distanceMatrix -> getDimension();
		int firstContigEdgeId = firstContig->getEdgeMatrixId();
		int secondContigEdgeId = secondContig->getEdgeMatrixId();
		for (int i = 0; i < firstPos; i++){
			int distanceMatrixValue = distanceMatrix -> getFieldValue(i, firstPos);
			if (i == 0){
				int index1 = edgeMatrix->getHeaderId(firstContig);
				int edgeMatrixValue = edgeMatrix -> getFieldValue(0, index1);
				if (edgeMatrixValue != NO_EDGE_CONSTANT)
					if (distanceMatrixValue >= edgeMatrixValue){
						numberOfUnhappyEdges++;
						if (numberOfUnhappyEdges >= partialScaffold->getNumberOfUnhappyEdges())
							return false;
					}
			}
			else{
				Contig * firstCont = partialScaffold->innerContigs[i-1];
				int index1 = edgeMatrix->getHeaderId(partialScaffold->innerContigs[i]);
				int index2 = edgeMatrix->getHeaderId(firstContig);
				int edgeMatrixValue = edgeMatrix -> getFieldValue(index2, index1);
				if (edgeMatrixValue != NO_EDGE_CONSTANT)
					if (distanceMatrixValue >= edgeMatrixValue){
						numberOfUnhappyEdges++;
						if (numberOfUnhappyEdges >= partialScaffold->getNumberOfUnhappyEdges())
							return false;
					}
			}

		}
		for (int i = secondPos + 1; i < sX; i++){
			if (i <= (sX - 2)){
				int index1 = edgeMatrix->getHeaderId(secondContig);
				int index2 = edgeMatrix->getHeaderId(partialScaffold->innerContigs[i - 1]);
				int distanceMatrixValue = distanceMatrix -> getFieldValue(secondPos, i);
				int edgeMatrixValue = edgeMatrix -> getFieldValue(index2, index1);
				if (edgeMatrixValue != NO_EDGE_CONSTANT)
					if (distanceMatrixValue >= edgeMatrixValue){
						numberOfUnhappyEdges++;
						if (numberOfUnhappyEdges >= partialScaffold->getNumberOfUnhappyEdges())
						return false;
					}
			}
			else{
				int index1 = edgeMatrix->getHeaderId(secondContig);
				int distanceMatrixValue = distanceMatrix -> getFieldValue(secondPos, i);
				int edgeMatrixValue = edgeMatrix -> getFieldValue(sX-1, index1);
				if (edgeMatrixValue != NO_EDGE_CONSTANT)
					if (distanceMatrixValue >= edgeMatrixValue){
						numberOfUnhappyEdges++;
						if (numberOfUnhappyEdges >= partialScaffold->getNumberOfUnhappyEdges())
						return false;
					}
			}
			
		}
		return true;
	}
	
	int deleteSmallerContig(SubScaffold * partialScaffold, int i, int j){
		Contig * smallerContig = ((partialScaffold->innerContigs[i]->length) < (partialScaffold->innerContigs[j]->length)) ? partialScaffold->innerContigs[i] : partialScaffold->innerContigs[j];
		//smallerContig -> setInvisible();
		BFSSearch * graphConnectivityChecker = new BFSSearch();
		graphConnectivityChecker -> makePartialScaffoldVisible(partialScaffold);
		smallerContig -> setInvisible();
		int isGraphConnected =  graphConnectivityChecker -> graphSearch(partialScaffold, filter, scaffoldGraph, bfsStats);
		graphConnectivityChecker -> makePartialScaffoldInvisible(partialScaffold);
		delete graphConnectivityChecker;
		if (isGraphConnected){
			partialScaffold -> incrementNumberOfDeletedContigs();
			if ((partialScaffold->innerContigs[i]->length) < (partialScaffold->innerContigs[j]->length)){
				resetGapSize(partialScaffold->innerContigs[i]);
				partialScaffold->innerContigs[i]->setDeleted();
				partialScaffold->innerContigs[i]->setInvisible();
				cout << "Removing contig: " << partialScaffold->innerContigs[i]->name << endl; 
				partialScaffold->innerContigs.erase(partialScaffold->innerContigs.begin() + i);
			}
			else{
				resetGapSize(partialScaffold->innerContigs[j]);
				partialScaffold->innerContigs[j]->setDeleted();
				partialScaffold->innerContigs[j]->setInvisible();
				cout << "Removing contig: " << partialScaffold->innerContigs[j]->name << endl; 
				partialScaffold->innerContigs.erase(partialScaffold->innerContigs.begin() + j);
			}
			return 1;
		}
		return 0;
	}
	
	DistanceMatrix * handleMemory(DistanceMatrix * oldDistanceMatrix, int dimension){
		DistanceMatrix * newDistanceMatrix = new DistanceMatrix(dimension, dimension);
		delete oldDistanceMatrix;
		return newDistanceMatrix;
	}
	
	void expandPartialScaffold(SubScaffold * partialScaffold, OpenNodesQueue * openPartialScaffolds, 
								VisitedNodesMap * visitedPartialScaffolds, EdgeDataMatrix * edgeMatrix){
		int innerContigsSize = partialScaffold -> getSizeOfInnerContigs();
		partialScaffoldChanged = false;	
		int matrixDimensions = innerContigsSize + 2;
		DistanceMatrix * distanceMatrix = new DistanceMatrix(matrixDimensions, matrixDimensions);
		distanceMatrix -> fillMatrix(partialScaffold);
		//distanceMatrix -> printDistanceMatrix();
		if (innerContigsSize >= 2){
		int i = 0;
		while (i < (partialScaffold->getSizeOfInnerContigs() - 1)){
			unvalidEdges = 0;
			distanceMatrix -> updateMatrix(i, i+1, partialScaffold->innerContigs[i], partialScaffold->innerContigs[i+1]);
			//distanceMatrix -> printDistanceMatrix();
			simplestSwap(partialScaffold, i, i+1);
			if (!checkIfVisited(partialScaffold, visitedPartialScaffolds)){
				if (checkValidityNew(partialScaffold, distanceMatrix, edgeMatrix, i, i + 1, partialScaffold->innerContigs[i + 1], partialScaffold->innerContigs[i])){
					statsFile << *(partialScaffold->innerContigs[i + 1]->scaffoldID) + 2 << "\t" << partialScaffold->innerContigs[i + 1]->name;
					statsFile << "\t" << partialScaffold->innerContigs[i + 1]->length << "\t" << partialScaffold->innerContigs[i + 1]->rightGap;
					statsFile << "\t" << partialScaffold->innerContigs[i]->name << "\t" << partialScaffold->innerContigs[i]->length << "\t" << partialScaffold->innerContigs[i]->rightGap << endl;
					int deleted = deleteSmallerContig(partialScaffold, i, i + 1);
					//partialScaffoldChanged = true;
					distanceMatrix = handleMemory(distanceMatrix, partialScaffold->getSizeOfInnerContigs() + 2);
					distanceMatrix -> fillMatrix(partialScaffold);
					//distanceMatrix -> printDistanceMatrix();
					numberOfAmbiguousRegions++;
					if (deleted == 0)
						i++;
					else{
						partialScaffoldChanged = true;
						changed = true;
						if (i >= 1)
							i--; 
					}
				}
				else{
					distanceMatrix -> updateMatrixReverse(i, i+1, partialScaffold->innerContigs[i+1], partialScaffold->innerContigs[i]);
					simplestSwap(partialScaffold, i, i+1);
					i++;
				}
			}
		}
		}
		delete distanceMatrix;
		makeVisited(partialScaffold, visitedPartialScaffolds);
	}
	
	void simplestSwap(SubScaffold * child, int i, int j){
		Contig * pom = child->innerContigs[i];
		child->innerContigs[i] = child->innerContigs[j];
		child->innerContigs[j] = pom;
	}
	
	void printCurrentPartialScaffold(SubScaffold * partialScaffold){
		for (int i = 0; i < partialScaffold->innerContigs.size(); i++)
			cout << partialScaffold->innerContigs[i]->name << " ";
		cout << endl;
	}
	
	int makeSystemCall(SubScaffold * partialScaffold){
		system("src/gapEstimater/bin/gapEstimater src/partial_scaffold.scaf src/partial_edges.dat src/est_scaf.scaf -41");
		int order = 0;
		ifstream gapFile("src/est_scaf.scaf");
		string name;
		string orientation;
		int size;
		int gapLen;
		string headerLine;
		getline(gapFile, headerLine);
		while(gapFile >> name >> orientation >> size >> gapLen ){
			Contig * contigObject = filter -> getPointerToContigObject(name);
			if ((contigObject -> border) == 0)
				contigObject -> updateGapSize(gapLen);
		}
		gapFile.close();

	}

	/*int makeSystemCall(SubScaffold * partialScaffold){
                system("/home/gisv85/opera/gapEstimater/bin/gapEstimater partial_scaffold.scaf partial_edges.dat est_scaf.scaf -41");
                int order = 0;
                ifstream gapFile("est_scaf.scaf");
                string name;
                string orientation;
                int size;
                int gapLen;
                string headerLine;
                getline(gapFile, headerLine);
                Contig * endBorder = NULL;
                if (partialScaffold -> borderContigEnd != NULL)
                        endBorder = partialScaffold -> borderContigEnd;
                while(gapFile >> name >> orientation >> size >> gapLen ){
                        Contig * contigObject = filter -> getPointerToContigObject(name);
                        if (endBorder != NULL)
                                if (contigObject != endBorder)
                                        contigObject -> updateGapSize(gapLen);
                        }
		gapFile.close();
		}*/

	void generatePartialScaffoldFile(SubScaffold * partialScaffold){
		ofstream tempScaffoldFile;
		tempScaffoldFile.open("src/partial_scaffold.scaf");
		tempScaffoldFile << ">simulated_scaffold_1" << endl;
		if (partialScaffold -> borderContig != NULL){
			tempScaffoldFile << partialScaffold->borderContig->name<<"\t"<<partialScaffold->borderContig->orientation<<"\t"<<partialScaffold->borderContig->length<<"\t"<<partialScaffold->borderContig->rightGap<<endl;

		}
		for (int i = 0; i < partialScaffold -> innerContigs.size(); i++){
			if ((partialScaffold -> innerContigs[i] -> isDeleted()) == false)
				tempScaffoldFile << partialScaffold->innerContigs[i]->name<<"\t"<<partialScaffold->innerContigs[i]->orientation<<"\t"<<partialScaffold->innerContigs[i]->length<<"\t"<<partialScaffold->innerContigs[i]->rightGap<<endl;		
		}
		if (partialScaffold -> borderContigEnd != NULL){
			tempScaffoldFile << partialScaffold->borderContigEnd->name<<"\t"<<partialScaffold->borderContigEnd->orientation<<"\t"<<partialScaffold->borderContigEnd->length<<"\t"<<partialScaffold->borderContigEnd->rightGap<<endl;

		}
	}			
	
	SubScaffold * doBFS(SubScaffold * partialScaffold){
		OpenNodesQueue * openPartialScaffolds = new OpenNodesQueue();
		VisitedNodesMap * visitedPartialScaffolds = new VisitedNodesMap();
		EdgeDataMatrix * edgeMatrix = new EdgeDataMatrix(partialScaffold, filter);
		edgeMatrix -> fillEdgeMatrix(partialScaffold, scaffoldGraph);
		//edgeMatrix -> printEdgeMatrix();
		vector <Contig*> scaffoldPart = partialScaffold->innerContigs;
		insertBack(partialScaffold, openPartialScaffolds);
		errorDetected = false;
		while (!(openPartialScaffolds->openNodes.empty())){
			SubScaffold * currentSub = removeHead(openPartialScaffolds);
			if (goalFunction(currentSub)) return currentSub;
			if (!(checkIfVisited(currentSub, visitedPartialScaffolds))){
				expandPartialScaffold(currentSub, openPartialScaffolds, visitedPartialScaffolds, edgeMatrix);
			}
		}
		if (partialScaffoldChanged){
			generatePartialScaffoldFile(partialScaffold);
			//makeSystemCall(partialScaffold);
			errorDetected = true;
		}
		delete edgeMatrix;
		delete openPartialScaffolds;
		delete visitedPartialScaffolds;
		
	}
	
	~LocalSearch(){
		delete data;
		delete filter;
		delete scaffoldGraph;
		delete edge_loader;
	}
	
	LocalSearch(const char * clusterFileName, const char * scaffoldFileName, const char * libFileName){
		numberOfAmbiguousRegions = 0;
		numberOfInitialUnhappyEdges = 0;
		bfsStats = new ofstream();
		bfsStats -> open("src/bfs_statistics.txt");
		statsFile.open("src/initial_stats.txt");
		statsFile << "Scaffold ID" << "\t" << "First contig" << "\t" << "Length" << "\t" << "Gap size" << "\t" << "Second contig" << "\t" << "Length" << "\t" << "Gap size" << endl;
		//cout << "edge" << endl;		
		edge_loader = new EdgeLoader();
		contigData = edge_loader -> loadContigDataToMap(scaffoldFileName);
		edge_loader -> getEdgesFromClusterFile(clusterFileName, edgeData);
		edgeData = edge_loader -> filterLessSupportedEdges(5, edgeData);
		//cout << "loader: " << endl;
		loader = new Loader();
		loader -> loadData(scaffoldFileName);
		//loader -> printData();
		loader -> iterateAndConstructPartialScaffolds(libFileName);
		//loader -> printAllBorderContigs();
		loader -> connectContigsWithPartialScaffolds();
		
		filter = new Filter();
		filter -> constructFilterMap(&(loader->scaffolds));
		edgeData = filter -> filterScaffoldConnectingEdges(edgeData);
		edgeData = filter -> filterSubscaffoldConnectingEdges(edgeData);
		//filter -> printValidEdges(edgeData);
		
		scaffoldGraph = new ScaffoldGraph(contigData, edgeData);
		scaffoldGraph -> printOutScaffoldGraph();

		data = loader;
		
		iterateOverPartialScaffolds();
		
		cout << "Number of ambiguous regions: " << numberOfAmbiguousRegions << endl;
		cout << "Number of initially unvalid edges: " << numberOfInitialUnhappyEdges << endl;
		
		statsFile.close();
		bfsStats -> close();
		delete bfsStats;
		
		
	}
};

#endif /*LOCAL_SEARCH_H*/
