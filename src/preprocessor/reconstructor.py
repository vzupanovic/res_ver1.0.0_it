# simple module which reconstructs the  merged regions 
# stored inside *.p files. It is called by rescaffold.cpp
# at the end of rescaffolding in order to reconstruct initial
# contigs and create valid output fasta file.

import pickle
import sys
import os.path

class Reconstructor:
	def __init__(self):
		#self. scaffoldFile = scaffoldFile
		self.contigInfo = pickle.load(open("src/preprocessor/all_contigs.p","rb"))
		self.allRegions = pickle.load(open("src/preprocessor/regions.info.p", "rb"))
		self.reconstructScaffolds()

	def reconstructScaffolds(self):
		self.scaffoldFileInput = open("src/preprocessor/res_scaffolds.scaf", "r")
		self.scaffoldFileOutput = open("src/preprocessor/res_scaffold.final.scaf", "w")
		data = self.scaffoldFileInput.readlines()
		for line in data:
			if (line[0] != ">"):
				line = line.strip()
				temp = line.split("\t")
				if temp[0][-1] == "M":
					lastGap = temp[3]
					region = self.allRegions[temp[0][:-1]]
					self.iterateOverRegions(temp[0][:-1], region, lastGap)
				else:
					self.scaffoldFileOutput.write(line + "\n")
			else:
				self.scaffoldFileOutput.write(line)
		self.scaffoldFileInput.close()
		self.scaffoldFileOutput.close()
	
	def iterateOverRegions(self, regionCentre, region, lastGap):
		region = [regionCentre] + region
		lastContig = region[-1]
		for contig in region:
			data = self.contigInfo[contig]
			data = list(data)
			data = [contig] + data
			if contig == lastContig:
				data[-1] = lastGap
			self.writeListToFile(data)	
		

	def writeListToFile(self, list):
		toWrite = list[0]+"\t"+list[1]+"\t"+list[2]+"\t"+list[3]+"\n"
		self.scaffoldFileOutput.write(toWrite)

if __name__ == "__main__":
	reconstructor = Reconstructor()
